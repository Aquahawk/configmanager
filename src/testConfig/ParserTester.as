package testConfig 
{
	import com.aquahawk.configmanager.ParsingError;
	/**
	 * ...
	 * @author Aquahawk
	 */
	public class ParserTester extends Tester 
	{
		
		public function ParserTester() 
		{
			var testCase:XmlTestCase = new XmlTestCase();
			
			//0
			/*********************************************************************************************/
			testCase = new XmlTestCase();
			testCase.xml =	<projectorConfig>
								<vlcConfig>
									<vlcPath>
										C:/Program Files (x86)/VideoLAN/VLC/vlc.exe1
									</vlcPath>
									<commandLineArgs>
										<arg>
											-I
										</arg>
										
										<arg>
											telnet
										</arg>
										
										<arg>
											--telnet-port
										</arg>
										
									</commandLineArgs>
								</vlcConfig>
								<VectorVectorVlcConfig>
									<test>
										<vlcConfig>
											<vlcPath>
												C:/Program Files (x86)/VideoLAN/VLC/vlc.exe2
											</vlcPath>
											<commandLineArgs>
												<arg>
													-I
												</arg>
												
												<arg>
													telnet
												</arg>
												
												<arg>
													--telnet-port
												</arg>
												
											</commandLineArgs>
										</vlcConfig>
										<vlcConfig>
											<vlcPath>
												C:/Program Files (x86)/VideoLAN/VLC/vlc.exe3
											</vlcPath>
											<commandLineArgs>
												<arg>
													-I
												</arg>
												
												<arg>
													telnet
												</arg>
												
												<arg>
													--telnet-port
												</arg>
												
											</commandLineArgs>
										</vlcConfig>
									</test>
									<test>
									</test>
								</VectorVectorVlcConfig>
								
								
							</projectorConfig>
			testCase.configClass = Config;
			testCases.push(testCase);
			
			//1
			/*********************************************************************************************/
			
			testCase = new XmlTestCase();
			testCase.xml =	<projectorConfig>
								<vlcConfig>
									<vlcPath>
										C:/Program Files (x86)/VideoLAN/VLC/vlc.exe1
									</vlcPath>
									<vlcPath>
										C:/Program Files (x86)/VideoLAN/VLC/vlc.exe1
									</vlcPath>
									<commandLineArgs>
										<arg>
											-I
										</arg>
										
										<arg>
											telnet
										</arg>
										
										<arg>
											--telnet-port
										</arg>
										
									</commandLineArgs>
								</vlcConfig>
								<VectorVectorVlcConfig>
									<test>
										<vlcConfig>
											<vlcPath>
												C:/Program Files (x86)/VideoLAN/VLC/vlc.exe2
											</vlcPath>
											<commandLineArgs>
												<arg>
													-I
												</arg>
												
												<arg>
													telnet
												</arg>
												
												<arg>
													--telnet-port
												</arg>
												
											</commandLineArgs>
										</vlcConfig>
										<vlcConfig>
											<vlcPath>
												C:/Program Files (x86)/VideoLAN/VLC/vlc.exe3
											</vlcPath>
											<commandLineArgs>
												<arg>
													-I
												</arg>
												
												<arg>
													telnet
												</arg>
												
												<arg>
													--telnet-port
												</arg>
												
											</commandLineArgs>
										</vlcConfig>
									</test>
									<test>
									</test>
								</VectorVectorVlcConfig>
								
								
							</projectorConfig>

			testCase.errorCode = ParsingError.IN_XML_BUT_NOT_IN_CONFIG;
			testCase.configClass = Config;
			testCases.push(testCase);
			
			//2
			/*********************************************************************************************/
			
			testCase = new XmlTestCase();
			testCase.xml =	<projectorConfig>
								<vlcConfig>
									<commandLineArgs>
										<arg>
											-I
										</arg>
										
										<arg>
											telnet
										</arg>
										
										<arg>
											--telnet-port
										</arg>
										
									</commandLineArgs>
								</vlcConfig>
								<VectorVectorVlcConfig>
									<test>
										<vlcConfig>
											<vlcPath>
												C:/Program Files (x86)/VideoLAN/VLC/vlc.exe2
											</vlcPath>
											<commandLineArgs>
												<arg>
													-I
												</arg>
												
												<arg>
													telnet
												</arg>
												
												<arg>
													--telnet-port
												</arg>
												
											</commandLineArgs>
										</vlcConfig>
										<vlcConfig>
											<vlcPath>
												C:/Program Files (x86)/VideoLAN/VLC/vlc.exe3
											</vlcPath>
											<commandLineArgs>
												<arg>
													-I
												</arg>
												
												<arg>
													telnet
												</arg>
												
												<arg>
													--telnet-port
												</arg>
												
											</commandLineArgs>
										</vlcConfig>
									</test>
									<test>
									</test>
								</VectorVectorVlcConfig>
								
								
							</projectorConfig>

			testCase.errorCode = ParsingError.IN_CONFIG_BUT_NOT_IN_XML;
			testCase.configClass = Config;
			testCases.push(testCase);
			
			//3
			/*********************************************************************************************/
			
			testCase = new XmlTestCase();
			testCase.xml =	<projectorConfig>
								<someTrash>
									C:/Program Files (x86)/VideoLAN/VLC/vlc.exe3
								</someTrash>
								<vlcConfig>
									<commandLineArgs>
										<arg>
											-I
										</arg>
										
										<arg>
											telnet
										</arg>
										
										<arg>
											--telnet-port
										</arg>
										
									</commandLineArgs>
								</vlcConfig>
								<VectorVectorVlcConfig>
									<test>
										<vlcConfig>
											<vlcPath>
												C:/Program Files (x86)/VideoLAN/VLC/vlc.exe2
											</vlcPath>
											<commandLineArgs>
												<arg>
													-I
												</arg>
												
												<arg>
													telnet
												</arg>
												
												<arg>
													--telnet-port
												</arg>
												
											</commandLineArgs>
										</vlcConfig>
										<vlcConfig>
											<vlcPath>
												C:/Program Files (x86)/VideoLAN/VLC/vlc.exe3
											</vlcPath>
											<commandLineArgs>
												<arg>
													-I
												</arg>
												
												<arg>
													telnet
												</arg>
												
												<arg>
													--telnet-port
												</arg>
												
											</commandLineArgs>
										</vlcConfig>
									</test>
									<test>
									</test>
								</VectorVectorVlcConfig>
								
								
							</projectorConfig>

			testCase.errorCode = ParsingError.IN_XML_BUT_NOT_IN_CONFIG;
			testCase.configClass = Config;
			testCases.push(testCase);
		}
		
	}

}