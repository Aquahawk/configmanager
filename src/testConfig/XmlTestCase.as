package testConfig 
{
	import com.aquahawk.configmanager.ConfigManager;
	import com.aquahawk.configmanager.IConfig;
	import com.aquahawk.configmanager.ParsingError;
	/**
	 * ...
	 * @author Aquahawk
	 */
	public class XmlTestCase extends TestCase 
	{
		
		public var xml:XML;
		public var configClass:Class;
		public var errorCode:int = -1;
		public override function run():Boolean
		{
			try
			{
				if ( 	ConfigManager.getXmlByConfig(
							ConfigManager.getConfigByXml(
								configClass, xml)) 
								!=
						ConfigManager.getXmlByConfig(
							ConfigManager.getConfigByXml(
								configClass,
								ConfigManager.getXmlByConfig(
									ConfigManager.getConfigByXml(configClass, xml))))
					)
				{
					trace ("Comparison after two serialisations failed");
					return false;
				}
			}
			catch (err:ParsingError)
			{
				if (errorCode != -1 && err.errorID == errorCode)
				{
					return true;
				}
				else
				{
					trace (err);
					return false;
				}
			}
			catch (err:Error)
			{
				trace (err);
				return false;
			}
			
			if (errorCode != -1 )
			{
				trace("No error, but error with code " + errorCode + " expected!");
				return false;
			}
			else
			{
				return true;
			}
		}
		
	}

}