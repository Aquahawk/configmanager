package testConfig 
{
	/**
	 * ...
	 * @author Aquahawk
	 */
	public class Tester 
	{
		protected var testCases:Vector.<TestCase>;
		
		public function Tester()
		{
			testCases = new Vector.<TestCase>();
		}
		
		public function runTests():Boolean
		{
			var errorsCount:int = 0;
			for (var i:int = 0; i < testCases.length; i++ )
			{
				if (testCases[i].run())
				{
					trace ("test " + i + " OK");
				}
				else
				{
					trace ("test " + i + " FAILED");
					errorsCount++;
				}
			}
			trace("DONE: " + (testCases.length - errorsCount) + "/" + testCases.length + "("+((testCases.length - errorsCount) / testCases.length)*100+ "%)");
			if (errorsCount)
			{
				trace("ERRORS: " + errorsCount+ "("+(errorsCount / testCases.length)*100+ "%)");
			}
			
			
			
			if (errorsCount == 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
	}

}