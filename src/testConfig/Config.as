package testConfig 
{
	import com.aquahawk.configmanager.IConfig;
	/**
	 * ...
	 * @author Aquahawk
	 */
	public class Config implements IConfig
	{
		public var vlcConfig:VlcConfig;
		public var VectorVectorVlcConfig:Vector.<Vector.<VlcConfig>>;
	}
}