package 
{
	import flash.display.Sprite;
	import testConfig.ParserTester;
	import flash.utils.getTimer;
	
	/**
	 * ...
	 * @author Aquahawk
	 */
	public class Main extends Sprite 
	{
		public function Main():void 
		{
			var parserTester:ParserTester = new ParserTester();
			var time:Number = getTimer();
			parserTester.runTests();
			trace("Test time: " + (getTimer() - time) +" ms");
		}	
	}
}