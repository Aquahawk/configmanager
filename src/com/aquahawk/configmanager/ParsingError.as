package com.aquahawk.configmanager 
{
	/**
	 * ...
	 * @author Aquahawk
	 */
	public class ParsingError extends Error 
	{
		public static const IN_XML_BUT_NOT_IN_CONFIG:int = 1;
		public static const IN_CONFIG_BUT_NOT_IN_XML:int = 2;
		public static const CLASS_IS_NOT_SUPPORTED:int = 3;
		public static const VALIDATION_FAILED:int = 4;
		
		public var error:Error;
		public function ParsingError(message:String, id:int) 
		{
			super("ConfigManager Parsing error: " + message, id)
		}
		
	}
}