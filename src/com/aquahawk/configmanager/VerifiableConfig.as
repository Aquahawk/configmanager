package com.aquahawk.configmanager 
{
	import com.aquahawk.rttiUtils.getAllAccessableProperties;
	import com.aquahawk.rttiUtils.Property;
	/**
	 * ...
	 * @author Aquahawk
	 */
	public class VerifiableConfig implements IConfig
	{
		protected function _verify():void
		{
			throw new Error("you must override _veryfy function, it will be called to verify your config object");
		}
		
		/*Этот метод нужен для того чтобы вызывать верификацию только текущего уровня. Для пользователя такое счастье не нужно.
		 * Во время парсинга после разбора текущего уровня он верифицируется, при этом очевидно что всё что ниже уже верифицировано.
		 * Поэтому не нужно верифицировать всё вниз с точки зрения производительности.
		 */
		internal function verifyCurrentLevelOnly():void
		{
			this._verify();
		}
	}
}