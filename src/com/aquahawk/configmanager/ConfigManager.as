package com.aquahawk.configmanager 
{
	import adobe.utils.CustomActions;
	import com.aquahawk.rttiUtils.getAllAccessableProperties;
	import com.aquahawk.rttiUtils.getVectorElementType;
	import com.aquahawk.rttiUtils.Property;
	import flash.utils.getQualifiedClassName;
	/**
	 * ...
	 * @author Aquahawk
	 */
	
	// TODO: 
	
	// сделать поддержку null(продумать как в конфиге казать что объекта нет и нет сознательно)
	// написать тест для валидируемого конфига, написать тест обновления xml
	// сделать документацию к эксепшнам (возможно есть специальные теги которые показывают какие эксепшены может кидать этот класс)
	// добавить поддержку uint, xml, 
	// добавить строгую типизацию, т.е. чтобы булеан проверялся и допускал только два значения, чтобы инты не содержали посторониих символов, и т.п. 6cef42e9-1ba5-41ce-a4be-2991d621a5c5 проверить то, что этот xml объект является простым, т.е. не иерархическим
	// hasSimpleContent и hasComplexContent для проверки того чтобы в строку или другой простой тип не ушёл xml целиком.
	// добаить проверку на null в передаваемых объектах
	
	
	// перенести RttiUtils в методы статического класса, тогда будет и где лишнее всё хранить и импортить удобнее будет и flashdevelop с ними лучше работает.
	// в RttiUtils добавить получение всех статических констант класса, и проверку есть ли такое значение среди всех статических констант данного класса
	// написать тесты для rttiUtils, выделить их в отдельную библиотеку
	
	
	// Улучшения
	
	/* Рассмотреть защиту от циклических ссылок, чтобы нельзя было в конфиг его же полем положить его самого. 
		Так можно сделать только при заполнении его руками(хотя при наличии ссылок в xml это тоже будет возможно.
		Для защиты применить вектор объектов ссылочного типа и складывать туда каждый следующий объект, и проверять нет ли там таких-же.*/
	// добавить в property указатель на объект от которого это проперти взято и добавить геттер value который возьмёт это поле от объекта
	// улучшить отображение ошибок, для этого сделать фунцию которая выдаст строку из родителей этого элемента(Xpath) тип можно узнавать через typeOf
	// добавить возможность делать ссылки между элементами
	// сделать возможность копировать конфиги (пока это реализуется выгрузкой в xml и загрузкой обратно)
	// оптимизировать функции получения инфы путём кеширования результатов
	// сделать enum
	
	
	
	// Разное
	
	// выделить тестер в отдельный маленький фреймвёрк
	
	
	
	// Замечания по работе парсера
	
	/* 
	 * 
	 * Валидация происходит в неопределённом порядке, а не в том что xml
	 * 
	 * */
	public class ConfigManager
	{
		public static function getConfigByXml(configClass:Class, xml:XML):IConfig
		{
			var configObject:IConfig = _getConfigByXml(configClass, xml) as IConfig;
			verifyConfig(configObject);
			return configObject;
		}
		
		public static function getXmlByConfig(configObject:IConfig):XML
		{
			verifyConfig(configObject);
			return _getXmlByConfig(configObject);
		}
		
		public static function verifyConfig(configObject:IConfig):void
		{
			for each (var property:Property in getAllAccessableProperties(configObject))
			{
				var propertyObject:IConfig = configObject[property.name] as IConfig;
				if (propertyObject)
				{
					verifyConfig(propertyObject);
				}
			}
			if (configObject is VerifiableConfig)
			{
				(configObject as VerifiableConfig).verifyCurrentLevelOnly();
			}
		}
		
		public static function updateXmlByConfig(xml:XML, configObject:IConfig):void
		{
			verifyConfig(configObject);
			_updateXmlByConfig(xml, configObject);
		}
		
		private static function _updateXmlByConfig(xml:XML, configObject:Object):void
		{
			var item:XML;
			var items:XMLList;
			var itemFound:Boolean;
			
			if (configObject is IConfig)
			{
				var allAccessableProperties:Vector.<Property> = getAllAccessableProperties(configObject);
				/*
				 * Тут важно отметить что мы перебираем ноды в XML и уже потом ищем для них записываемое свойство класса.
				 * Это нужно чтобы парсить xml документ строго сверху вниз. Это гарантирует остановку разбора в случае 
				 * обнаружения ошибки. Также это сделано для будущих модификаций и возможности делать ссылки из одной ноды на другие.
				 * */
				for each (item in xml.elements()) 
				{
					itemFound = false;
					for (var i:int=0; i < allAccessableProperties.length; i++ )
					{
						var property:Property = allAccessableProperties[i];
						if (item.name() == property.name)
						{
							_updateXmlByConfig(item, configObject[property.name]);
							allAccessableProperties.splice(i, 1);
							itemFound = true;
							break;
						}
					}
					if (!itemFound)
					{
						throw new ParsingError ("Item: " + item.name() + " specified in XML but not defined in config, or may be it specified more then one time at once!", ParsingError.IN_XML_BUT_NOT_IN_CONFIG);
					}
				}
				if (allAccessableProperties.length > 0)
				{
					throw new ParsingError("Items named: " + allAccessableProperties.join(", ") + " is defined in config, but not specified in XML!", ParsingError.IN_CONFIG_BUT_NOT_IN_XML);
				}
			}
			
			
			/**/
			else if (configObject is Vector.<*> || configObject is Vector.<Number> || configObject is Vector.<int> || configObject is Vector.<uint>)
			{
				var configVector:* = configObject.concat();
				
				for each (item in xml.elements())
				{
					_updateXmlByConfig(item, configVector.shift());
				}
				
				for each (var confObj:Object in configVector)
				{
					var insXml:XML = _getXmlByConfig(confObj);
					insXml.setName("item")
					xml.appendChild(insXml);
				}
			}
			else if (configObject is String) // 6cef42e9-1ba5-41ce-a4be-2991d621a5c5 
			{
				xml.* = configObject;
			}
			else if (configObject is Boolean) // 6cef42e9-1ba5-41ce-a4be-2991d621a5c5
			{
				xml.* = configObject;
			}
			/*else if (configObject is int || configObject is uint) // 6cef42e9-1ba5-41ce-a4be-2991d621a5c5
			{
				xml.* = configObject;
			}*/
			else if (configObject is Number) // 6cef42e9-1ba5-41ce-a4be-2991d621a5c5
			{
				xml.* = configObject;
			}
			else
			{
				throw new ParsingError("Class is not supported", ParsingError.CLASS_IS_NOT_SUPPORTED);
			}
			
			/*
			uint
			XML
			*/
		}
		
		private static function _getXmlByConfig(configObject:Object):XML
		{
			var xml:XML = <serializedConfig />;
			
			var item:XML;
			if (configObject is IConfig)
			{
				if (configObject is VerifiableConfig)
				{
					xml.@type = "VerifiableConfig";
				}
				else
				{
					xml.@type = "IConfig";
				}
				
				var allAccessableProperties:Vector.<Property> = getAllAccessableProperties(configObject);

				for each (var property:Property in allAccessableProperties)
				{
					item = _getXmlByConfig(configObject[property.name]);
					item.setName(property.name);
					xml.appendChild(item);
				}
			}
			else if (configObject is Vector.<*> || configObject is Vector.<Number> || configObject is Vector.<int> || configObject is Vector.<uint>)
			{
				xml.@type = "Vector";
				for each (var vectorItem:Object in configObject)
				{
					item = _getXmlByConfig(vectorItem);
					item.setName("value");
					xml.appendChild(item);
				}
			}
			else if (configObject is String) // 6cef42e9-1ba5-41ce-a4be-2991d621a5c5 
			{
				xml.@type = "String";
				xml.appendChild(configObject);
			}
			else if (configObject is Boolean) // 6cef42e9-1ba5-41ce-a4be-2991d621a5c5
			{
				xml.@type = "Boolean";
				xml.appendChild(configObject);
			}
			/*else if (configObject is int || configObject is uint) // 6cef42e9-1ba5-41ce-a4be-2991d621a5c5
			{
				xml.@type = "int";
				xml.appendChild(configObject);
			}*/
			else if (configObject is Number) // 6cef42e9-1ba5-41ce-a4be-2991d621a5c5
			{
				xml.@type = "Number";
				xml.appendChild(configObject);
			}
			else
			{
				throw new ParsingError("Class " + getQualifiedClassName(configObject) + " is not supported", ParsingError.CLASS_IS_NOT_SUPPORTED);
			}
			
			/*
			uint
			XML
			*/
			
			return xml;
		}
		
		private static function _getConfigByXml(configClass:Class, xml:XML):Object
		{
			var item:XML;
			var items:XMLList;
			var itemFound:Boolean;
			var configObject:Object = new configClass();
			
			if (configObject is IConfig)
			{
				var allAccessableProperties:Vector.<Property> = getAllAccessableProperties(configObject);
				/*
				 * Тут важно отметить что мы перебираем ноды в XML и уже потом ищем для них записываемое свойство класса.
				 * Это нужно чтобы парсить xml документ строго сверху вниз. Это гарантирует остановку разбора в случае 
				 * обнаружения ошибки. Также это сделано для будущих модификаций и возможности делать ссылки из одной ноды на другие.
				 * */
				for each (item in xml.elements()) 
				{
					itemFound = false;
					for (var i:int=0; i < allAccessableProperties.length; i++ )
					{
						var property:Property = allAccessableProperties[i];
						if (item.name() == property.name)
						{
							configObject[property.name] = _getConfigByXml(property.classObj, item);
							allAccessableProperties.splice(i, 1);
							itemFound = true;
							break;
						}
					}
					if (!itemFound)
					{
						throw new ParsingError ("Item: " + item.name() + " specified in XML but not defined in config, or may be it specified more then one time at once!", ParsingError.IN_XML_BUT_NOT_IN_CONFIG);
					}
				}
				if (allAccessableProperties.length > 0)
				{
					throw new ParsingError("Items named: " + allAccessableProperties.join(", ") + " is defined in config, but not specified in XML!", ParsingError.IN_CONFIG_BUT_NOT_IN_XML);
				}
			}
			else if (configObject is Vector.<*> || configObject is Vector.<Number> || configObject is Vector.<int> || configObject is Vector.<uint>)
			{
				for each (item in xml.elements())
				{
					configObject.push(_getConfigByXml(getVectorElementType(configClass), item));
				}
			}
			else if (configObject is String) // 6cef42e9-1ba5-41ce-a4be-2991d621a5c5 
			{
				configObject = xml.toString();
			}
			else if (configObject is Boolean) // 6cef42e9-1ba5-41ce-a4be-2991d621a5c5
			{
				configObject = (xml.toString() == "true");
			}
			/*else if (configObject is int || configObject is uint) // 6cef42e9-1ba5-41ce-a4be-2991d621a5c5
			{
				configObject = parseInt(xml.toString());
			}*/
			else if (configObject is Number) // 6cef42e9-1ba5-41ce-a4be-2991d621a5c5
			{
				configObject = parseFloat(xml.toString());
			}
			else
			{
				throw new ParsingError("Class " + configClass + " is not supported", ParsingError.CLASS_IS_NOT_SUPPORTED);
			}
			
			/*
			uint
			XML
			*/
			return configObject;
		}
	}
}