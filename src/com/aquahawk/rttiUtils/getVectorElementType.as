package com.aquahawk.rttiUtils
{
	import flash.utils.describeType;
	import flash.utils.getDefinitionByName;
	/**
	 * ...
	 * @author Aquahawk
	 */
	public function getVectorElementType(testVector:Class):Class
	{ 
		var vectorTypeName:String = describeType(testVector).@name.toString();
		var elementTypeName:String = vectorTypeName.substr(vectorPrefixLength, vectorTypeName.length - vectorPrefixLength - vectorPostfixLength); 
		return getDefinitionByName(elementTypeName) as Class;
	}
}

import flash.utils.describeType;
import flash.utils.escapeMultiByte;

var vectorPrefixLength:int;
var vectorPostfixLength:int;
var vectorDescriptionParts:Array;
vectorDescriptionParts = (describeType(new Vector.<*> ).@name.toString()).split("*");
vectorPrefixLength = vectorDescriptionParts[0].length;
vectorPostfixLength = vectorDescriptionParts[1].length;
