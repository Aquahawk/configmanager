package com.aquahawk.rttiUtils 
{
	import flash.utils.describeType;
	import flash.utils.getDefinitionByName;
	/**
	 * ...
	 * @author Aquahawk
	 */
	public function getAllReadableProperties(obj:Object):Vector.<Property>
	{
		var result:Vector.<Property> = new Vector.<Property>();
		var objDesc:XML = describeType(obj);
		var properties:XMLList = objDesc.accessor.(@access == "readwrite" || @access == "readonly") + objDesc.variable;
		for each (var propertyNode:XML in properties)
		{
			var property:Property = new Property();
			property.name = (propertyNode.@name).toString();
			property.classObj = getDefinitionByName(propertyNode.@type.toString()) as Class;
			result.push(property);
		}
		return result;
	}

}